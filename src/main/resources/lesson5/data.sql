insert into publishers(name) values
	('Москва'),
	('Эксмо'),
	('Самиздат');

insert into book(name, isbn, year, publisher_id) values
	('С/С++', 		'435-234', 		2013, 1),
	('Java 5', 		'42364-3', 		2011, 2),
	('Python 3.2', 	'656-43', 		2019, 1),
	('Regexp', 		'423-4523',		2003, 3),
	('SQL', 		'423-411', 		2006, 1);

insert into author(name, last_name, age) values
	('Steve', 'Kovi', 43),
	('Peter', 'P', 41),
	('True', 'False', 33),
	('James', 'N', null),
	('Johny', 'T', 35);

insert into author_books(author_id, book_id) values
	(1, 1),
	(1, 2),
	(2, 1),
	(3, 3),
	(2, 3),
	(4, 3),
	(5, 1),
	(1, 5),
	(2, 5),
	(4, 4);

