-- avg, min, max, sum, count
select count(*) from author;
select count(*) from author where age > 36;
select count(age) from author;

select round(avg(age), 2) from author;

select name from book where year = (select min(year) from book);

-- publisher name | count of books
-- publisher name | count of books
select name, min(age) from author;

select p.name, count(b.*) from publishers p
join book b on p.id = b.publisher_id
group by p.name
having count(*) > 1;

select name, min(age) from author
group by name;

