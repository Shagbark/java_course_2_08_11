create table publishers
(
	-- integer auto increment
	id serial primary key,
	name varchar not null unique
);

create table book
(
	id serial primary key,
	isbn varchar(30) not null unique,
	name varchar not null,
	year integer not null,
	publisher_id integer not null,
	constraint publisher_id_fkey foreign key (publisher_id) references publishers(id)
);

create table book_details
(
	id serial primary key,
	pages_count integer,
	genre varchar not null,
	book_id integer not null unique,
	constraint book_id_fkey foreign key (book_id) references book(id)
);

create table author
(
	id serial primary key,
	name varchar(100) not null,
	last_name varchar(100) not null,
	age integer
);

create table author_books
(
	author_id integer not null,
	book_id integer not null,
	constraint author_books_pkey primary key (author_id, book_id),
	constraint author_id_fkey foreign key (author_id) references author(id),
	constraint book_id_fkey foreign key (book_id) references book(id)
);



