create table seq (
	id integer primary key, -- serial
	name varchar
);

create sequence seq_id_sequence
	increment by 1
	start with 1000
	cache 10;

 -- 76
 -- 77 - 89
insert into seq (id, name)
values
	(nextval('seq_id_sequence'), 'Name'),
	(nextval('seq_id_sequence'), 'Name 2');

insert into seq (id, name)
values
	(1, 'Name 1');

select * from seq;
select * from book_id_seq;

select currval('seq_id_sequence');