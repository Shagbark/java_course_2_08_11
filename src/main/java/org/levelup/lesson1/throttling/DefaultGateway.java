package org.levelup.lesson1.throttling;

public class DefaultGateway implements Gateway {

    private int requestCounter = 0;

    @Override
    @Throttling
    public void processRequest(Object request) {
        System.out.println("Количество запросов: " + ++requestCounter);
    }

}
