package org.levelup.lesson1.throttling;

public interface Gateway {

    void processRequest(Object request);

}
