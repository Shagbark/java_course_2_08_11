package org.levelup.lesson1.throttling;

public class App {

    public static void main(String[] args) {
        DefaultGateway defaultGateway = new DefaultGateway();

        ThrottlingAnnotationProcessor processor =
                new ThrottlingAnnotationProcessor();
        // Запускаем процесс поиска аннотации @Throttling в классе DefaultGateway.
        // На выходе мы получаем новый объект, который внутри себя содержит объект defaultGateway (прокси-объект)
        Gateway gateway = (Gateway) processor.processAnnotations(defaultGateway);
        // Вызывается метод invoke (из InvocationHandler), который внутри себя уже вызывает оригинальный метод
        // processRequest из класса DefaultGateway
        gateway.processRequest(new Object());

    }

}
