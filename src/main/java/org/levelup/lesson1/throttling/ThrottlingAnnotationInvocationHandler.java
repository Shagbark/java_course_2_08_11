package org.levelup.lesson1.throttling;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Класс представляет собой перехватчик вызовов метода
 * Если мы создаем прокси-объект (через Proxy.newProxyInstance()), то теперь все вызовы
 * методов оригинального объекта будут проходить через метод invoke, который описан в этом классе.
 *
 * К примеру, был метод method() у класса. Мы создали proxy. Теперь на самом деле
 * вызывается метод invoke, который внутри себя вызывает method().
 *
 */
@SuppressWarnings("ALL")
public class ThrottlingAnnotationInvocationHandler implements InvocationHandler {

    /**
     * Метод, который помечен аннотацией @Throttling.
     * В нашем случае - это метод processRequest(Object request)
     */
    private Method annotatedMethod;

    /**
     * Объект класса, у которого есть хотя бы один метод, который помечен аннотацией @Throttling.
     * В нашем случае - это объект класса DefaultGateway.
     */
    private Object object;

    public ThrottlingAnnotationInvocationHandler(Method annotatedMethod, Object object) {
        this.annotatedMethod = annotatedMethod;
        this.object = object;
    }

    /**
     * В этом методе мы определяем поведение метода, которого хотели изменить. По сути
     * здесь происходит то же самое, что и при переопределении метода в наследовании. Только
     * в данном случае никакого наследования нет. Мы просто меняем поведение.
     *
     * @param proxy прокси-объект (тот объект, который создается при Proxy.newProxyInstance()
     * @param method метод, который в данный момент вызывается
     * @param args аргументы метода, который в данный момент вызывается
     * @return результат работы метода (если метод void, то можно просто вернуть null)
     * @throws Throwable ошибка работы прокси
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // Здесь мы определили, что мы хотим сделать, если вызывается метод,
        // у которого есть аннотация @Throttling
        if (method.getName().equals(annotatedMethod.getName())) {
            System.out.println("Данный метод помечен аннотацией @Throttling");
        } else {
            System.out.println("У метода " + method.getName() + " нет аннотации @Throttling");
        }

        // Вызов оригинального метода (в нашем случае метода processRequest() из класса DefaultGateway
        // Эквивалентно:
        //  Gateway g = new DefaultGateway()
        //  g.processRequest(new Object());

        // method - метод, который вызывается
        // object - объект, у которого вызывается метод method
        // args - аргументы, которые передаются в метод
        return method.invoke(object, args);
    }

}
