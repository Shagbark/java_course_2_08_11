package org.levelup.lesson1.throttling;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@SuppressWarnings("ALL")
public class ThrottlingAnnotationProcessor {

    public Object processAnnotations(Object classObjectWithAnnotatedMethod) {
        // Значение objectClass будет зависить от того объекта, который мы сюда передали
        //
        // Вызвали:
        //      .processAnnotations("Some string")
        // То тогда Class<?> будет объектом класса Class для String
        //
        // Вызвали:
        //      .processAnnotations(new DefaultGateway())
        // То тогда Class<?> будет объектом класса Class для DefaultGateway.
        Class<?> objectClass = classObjectWithAnnotatedMethod.getClass();

        // Получаем список методов, которые есть в классе
        Method[] methods = objectClass.getDeclaredMethods();
        for (Method method : methods) {
            // Ищем метод, который помечен аннотацией @Throttling
            // Для этого определяем, если у метода аннотация

            // Метод .getAnnotation принимает объект класса Class аннотации, который мы хотим найти
            Throttling annotation = method.getAnnotation(Throttling.class);
            if (annotation != null) {
                // Если annotation != null, то это означает, что у данного метода
                // проставлена аннотация (в данном случае @Throttling)

                // Далее мы создаем прокси-объект, чтобы в дальнейшем смогли переопределить
                // поведение метода с аннотацией @Throttling
                return createProxyObject(classObjectWithAnnotatedMethod, method, objectClass);
            }

        }

        // Если мы не нашли ни одного метода, у которого есть аннотация Throttling,
        // то тогда возвращаем тот же объект (без каких-либо изменений), который
        // нам передавали в метод
        return classObjectWithAnnotatedMethod;
    }

    private Object createProxyObject(Object object, Method method, Class<?> objectClass) {
        // Для того, чтобы переопределить поведение метода в классе (не прибегая к наследованию
        // и переопределению метода), нам нужно завернуть наш объект в свою оболочку.
        // Для этого используем механизм проксирования (создания прокси-объекта)

        return Proxy.newProxyInstance(
                objectClass.getClassLoader(),
                objectClass.getInterfaces(), // Вернет только те интерфейсы, которые явно прописаны в implements у этого класса
                new ThrottlingAnnotationInvocationHandler(method, object)
        );
    }

}
