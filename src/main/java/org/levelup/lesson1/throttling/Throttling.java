package org.levelup.lesson1.throttling;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@SuppressWarnings("ALL") // просто отключает warnings от компилятора
/**
 * Данная аннотация указывает, где может применяться наша аннотация
 */
@Target(ElementType.METHOD)
/**
 * Данная аннотация показывает в каком месте будет видна аннотация @Throttling.
 * SOURCE - аннотация видна только в исходном коде (удаляется после компиляции)
 * CLASS - аннотация видна в исходном коде и в байткоде (удаляется после старта программы)
 * RUNTIME - аннотация видна в исходном коде, в байткоде и во время работы программы (т.е. она не
 * удаляется)
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Throttling {
}
