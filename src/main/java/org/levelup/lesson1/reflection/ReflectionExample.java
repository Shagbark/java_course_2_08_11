package org.levelup.lesson1.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionExample {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        User userObject = new User();

        // Для того, чтобы получить объект класса Class<?> существует
        // 2 способа.

        // 1 способ - вызов getClass() у объекта нужного объекта
        Class<?> userClass = userObject.getClass();

        // 2 способ - использование литерала .class
        // Этот способ используется, когда нужно получить объект класса Class<?>,
        // но нет объекта этого класса (нет userObject)
        Class<?> userClassByLiteral = User.class;

        // Эти два способа возвращают одну и ту же ссылку на объект Class
        // Объекта класс создается в единственном экземпляре на определенный ClassLoader
        System.out.println("Сравнение ссылок на Class<User>: " + (userClass == userClassByLiteral));
        System.out.println();

        // Рефлексия
        // Получение списка полей объекта
        Field[] fields = userClass.getDeclaredFields();
        System.out.println("Поля класса User:");
        for (Field field : fields) {
            System.out.println(field.getName());
        }
        System.out.println();

        // Получение списка полей объекта
        Method[] methods = userClass.getDeclaredMethods();
        System.out.println("Методы класса User:");
        for (Method method : methods) {
            System.out.println(method.getName());
        }
        System.out.println();

        // Получение списка полей объекта
        Constructor[] constructors = userClass.getDeclaredConstructors();
        System.out.println("Конструкторы класса User:");
        for (Constructor constructor : constructors) {
            System.out.println(constructor.getName());
        }
        System.out.println();

        // Получение объекта Field по названию поля
        Field fieldName = userClass.getDeclaredField("name");
        // Без установки флага setAccessible(true), при попытки
        // записать или прочитать значение из не публичного поля (non-public field)
        // мы получаем IllegalAccessException
        fieldName.setAccessible(true);
        // Установка значение в определенное поле
        // Это эквивалентно записи: userObject.name = "Steve";
        fieldName.set(userObject, "Steve");
        // Чтение значения из определенного поля
        // Это эквивалентно записи: String value = userObject.name;
        String value = (String) fieldName.get(userObject);
        System.out.println("Значение поля name: " + value);
    }

}
