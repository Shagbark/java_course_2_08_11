package org.levelup.lesson1.reflection;

@SuppressWarnings("ALL")
public class User {

    private String name;
    public int age;

    public User() {}

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

}
