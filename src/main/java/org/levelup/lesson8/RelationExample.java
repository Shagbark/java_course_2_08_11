package org.levelup.lesson8;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.book.store.HibernateUtils;
import org.levelup.book.store.domain.Book;
import org.levelup.book.store.domain.BookDetails;
import org.levelup.book.store.domain.Publisher;

import java.util.List;

public class RelationExample {

    public static void main(String[] args) {
        SessionFactory factory = HibernateUtils.getFactory();
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

//        Publisher publisher = new Publisher("Издательство 1");
//        session.save(publisher);
//
//        Book book = new Book();
//        book.setIsbn("4253-53234");
//        book.setName("Book #1");
//        book.setYear(2019);
//
//        book.setPublisher(publisher);
//        session.save(book);
        Publisher publisher = session.get(Publisher.class, 1);
        Book book = new Book();
        book.setIsbn("74568-888-88-538");
        book.setName("Book #19");
        book.setYear(2018);
        book.setPublisher(publisher);

        BookDetails details = new BookDetails();
        details.setPagesCount(542);
        details.setGenre("Жанр 8");
        details.setBook(book);
        book.setDetails(details);

        session.persist(book);

        // select * from book
        Book b = session.createQuery("from Book where id = :bookId and name like :name" , Book.class)
                .setParameter("bookId", 1)
                .setParameter("name", "Book #%")
                .getSingleResult();
//        for (Book b : books) {
//            System.out.println(b.toString());
//        }


//        details.setBook(book);
//        session.save(details);

        transaction.commit();
        session.close();
        factory.close();
    }

}
