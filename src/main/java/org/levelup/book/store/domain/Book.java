package org.levelup.book.store.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "book")
public class Book {

    // hibernate_sequence
    @Id
    @SequenceGenerator(sequenceName = "book_id_seq", name = "book_seq_generator", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_seq_generator")
    private Integer id;

    @Column(name = "isbn", unique = true, nullable = false)
    private String isbn;

    private String name;

    private int year;

    @ManyToOne
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH }, mappedBy = "book")
    private BookDetails details;

}
