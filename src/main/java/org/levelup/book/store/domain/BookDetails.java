package org.levelup.book.store.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "book_details")
public class BookDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "pages_count")
    private Integer pagesCount;

    private String genre;

    @OneToOne
    @MapsId
    @JoinColumn(name = "book_id")
    private Book book;

}
