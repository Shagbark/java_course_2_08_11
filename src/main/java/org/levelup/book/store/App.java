package org.levelup.book.store;

import org.hibernate.SessionFactory;
import org.levelup.book.store.domain.Publisher;
import org.levelup.book.store.repository.PublisherRepository;

public class App {

    public static void main(String[] args) {
        SessionFactory factory = HibernateUtils.getFactory();

        PublisherRepository publisherRepository = new PublisherRepository(factory);

        Publisher publisher = publisherRepository.createPublisher("Москва 4.0");

        publisherRepository.updatePublisher(publisher.getId(), "Москва 5.0");

        factory.close();

    }

}
