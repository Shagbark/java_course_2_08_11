package org.levelup.lesson3.queue;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

// Thread safe - класс/метод можно использовать
// для нескольких потоков
public class ThreadSafeQueue {

    private ReentrantLock lock = new ReentrantLock();
    private ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private Condition empty = lock.newCondition();
    private Condition full = lock.newCondition();

//    private final Object empty = new Object();
//    private final Object full = new Object();

    private LinkedList<Runnable> tasks;
    private int maxSize;

    public ThreadSafeQueue(int maxSize) {
        this.maxSize = maxSize;
        this.tasks = new LinkedList<>();
    }

    // public synchronized ...
    // t1.queue.add
    // t2.queue.add
    // t3.queue.add
    public void add(Runnable task) throws InterruptedException {
//        synchronized (this) {
        lock.lock();
        try {
            while (tasks.size() == maxSize) {
                full.await();
            }
            tasks.addLast(task);
//            notifyAll();
            empty.signalAll();
        } finally {
            lock.unlock();
        }
//        }
    }

    public Runnable take() throws InterruptedException {
//            synchronized (this) {
        lock.lock();
        try {
            while (tasks.isEmpty()) {
//                wait();
                empty.await();
            }
            Runnable task = tasks.poll();
            // notifyAll();
            full.signalAll();
            return task;
        } finally {
            lock.unlock();
        }
//        }
    }

    public int getCurrentSize() {
        return tasks.size();
    }

}
