package org.levelup.lesson3.queue;

import java.util.concurrent.atomic.AtomicLong;

public class NonBlockingCounter {

    private AtomicLong counter = new AtomicLong();

    public void increment() {
        counter.incrementAndGet();
    }

    public long getCounter() {
        return counter.get();
    }

}
