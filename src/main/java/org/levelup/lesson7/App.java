package org.levelup.lesson7;

public class App {

    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("org.levelup.lesson7.BlockInitializations");

        new BlockInitializations();
        new BlockInitializations();
    }

}
