package org.levelup.lesson7;

public class BlockInitializations {

    static {
        System.out.println("Call static 1");
    }

    {
        System.out.println("Call 1");
    }

    public BlockInitializations() {
        System.out.println("Call constructor");
    }

    {
        System.out.println("Call 2");
    }

    static {
        System.out.println("Call static 2");
    }

}
