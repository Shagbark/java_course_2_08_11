package org.levelup.lesson6;

import java.sql.*;

// ACID
//
public class AuthorDatabaseConnection {

    public Connection getConnection() throws SQLException {
        // URL:
        //  jdbc:<vendor_name>://<host>:<port>/<database_name>?=;&
        return DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/book_store2", "postgres", "root"
        );
    }

    public void printAllAuthors() {
        System.out.println("Try to connect to database...");
        try (Connection connection = getConnection()) {
            System.out.println("Connected to database...");

            // Statement
            // PreparedStatement
            // CallableStatement
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery("select * from author;");
            while (result.next()) {
                // id, name, last_name, age
                String name = result.getString(2); // result.getString("name")
                String lastName = result.getString("last_name");
                int age = result.getInt(4);

                System.out.println(name + " " + lastName + " " + age);
            }

//            boolean isSelect = statement.execute("select * from author");
//            if (isSelect) {
//                ResultSet results = statement.getResultSet();
//                // code here
//            }

        } catch (SQLException exc) {
            throw new RuntimeException(exc);
        }
    }

    // delete from author where name = ?
    public void addAuthor(String name, String lastName, int age) {
        try (Connection connection = getConnection()) {

            PreparedStatement statement = connection
                    .prepareStatement("insert into author(name, last_name, age) values (?, ?, ?) ");
            statement.setString(1, name);
            statement.setString(2, lastName);
            statement.setInt(3, age);

            int rowsAffected = statement.executeUpdate();
            System.out.println(rowsAffected);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
