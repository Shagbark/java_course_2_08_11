package org.levelup.lesson6;

import java.sql.SQLException;

public class App {

    public static void main(String[] args) throws SQLException {
        AuthorDatabaseConnection connection = new AuthorDatabaseConnection();
        connection.printAllAuthors();

        connection.addAuthor("Robert", "Daud", 65);

        connection.printAllAuthors();

    }

}
