package org.levelup.lesson10;

import java.util.*;
import java.util.stream.Collectors;

public class StreamExamples {

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>(Arrays.asList(1, 2, 53, 34, 12, 53, 676, 76, 34));

        List<Integer> greater30 = integers.parallelStream()
//                .filter(element -> {
//                    return element > 30;
//                })
                .filter(element -> element > 30)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

        long count = integers.stream()
                // .map(el -> String.valueOf(el))
                // .filter(el -> Objects.nonNull(el) && !Objects.equals(el, null))
                .filter(Objects::nonNull)
                // .filter(el -> Objects.equals(el, null))
                .map(String::valueOf)
                .peek(System.out::println)
                // .map(this::valueOf)
                // Obj o = new Obj()
                // .map(o::valueOf)
                .filter(str -> str.length() == 3)
                .count();

        // Map<Integer, List<Integer>>
        Map<Integer, List<Integer>> collect = integers.stream()
                .collect(Collectors.groupingBy(el -> String.valueOf(el).length()));
        collect.forEach((k, v) -> System.out.println(k + " " + v));

//        List<Integer> filtered = new ArrayList<>();
//        for (Integer integer : integers) {
//            if (integer > 30) {
//                filtered.add(integer);
//            }
//        }
//        Iterator<Integer> iterator = integers.iterator();
//        while (iterator.hasNext()) {
//            if (iterator.next() < 30) {
//                iterator.remove();
//            }
//        }
        // integers - value > 30


    }

    private static void m(List<Integer> integers) throws Exception {
//        integers.forEach(el -> {
//            try {
//                m2(el);
//            } catch (Exception e) {
//                throw new RuntimeException(e);
//            }
//        });
        for (Integer integer : integers) {
            m2(integer);
        }
    }

    private static void m2(Integer el) throws Exception {
        throw new Exception();
    }

}
